﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Kinect.Toolkit;
using Microsoft.Kinect;
using Microsoft.Kinect.Toolkit.Controls;

namespace Etape1
{
    public sealed class KinectSingleton
    {
        private static volatile KinectSingleton instance;
        private KinectSensorChooser sensorChooser;
        private KinectSensorChooserUI sensorChooserUi;
        private KinectRegion kinectRegion;
        private KinectSensor kinectSensor;
        private static object syncRoot = new Object();

        private KinectSingleton()
        {
            this.sensorChooser = new KinectSensorChooser();
        }

        public void init(KinectSensorChooserUI sensorChooserUi, KinectRegion kinectRegion)
        {
            //sensorChooser.Stop();
            this.kinectRegion = kinectRegion;
            if (kinectSensor != null)
                kinectRegion.KinectSensor = kinectSensor;
            this.sensorChooser.KinectChanged += SensorChooserOnKinectChanged;

            this.sensorChooserUi = sensorChooserUi;
            this.sensorChooserUi.KinectSensorChooser = this.sensorChooser;
            this.sensorChooser.Start();
        }

        private void SensorChooserOnKinectChanged(object sender, KinectChangedEventArgs args)
        {
            bool error = false;
            if (args.OldSensor != null)
            {
                try
                {
                    args.OldSensor.DepthStream.Range = DepthRange.Default;
                    args.OldSensor.SkeletonStream.EnableTrackingInNearRange = false;
                    args.OldSensor.DepthStream.Disable();
                    args.OldSensor.SkeletonStream.Disable();
                }
                catch (InvalidOperationException)
                {
                    // KinectSensor might enter an invalid state while enabling/disabling streams or stream features.
                    // E.g.: sensor might be abruptly unplugged.
                    error = true;
                }
            }

            if (args.NewSensor != null)
            {
                try
                {
                    args.NewSensor.DepthStream.Enable(DepthImageFormat.Resolution640x480Fps30);
                    args.NewSensor.SkeletonStream.Enable();

                    try
                    {
                        //args.NewSensor.DepthStream.Range = DepthRange.Near;
                        args.NewSensor.SkeletonStream.EnableTrackingInNearRange = true;
                        args.NewSensor.SkeletonStream.TrackingMode = SkeletonTrackingMode.Seated;
                        args.NewSensor.DepthStream.Range = DepthRange.Near;
                    }
                    catch (InvalidOperationException e)
                    {
                        // Non Kinect for Windows devices do not support Near mode, so reset back to default mode.
                        args.NewSensor.DepthStream.Range = DepthRange.Default;
                        args.NewSensor.SkeletonStream.EnableTrackingInNearRange = true;
                        error = false;
                    }
                }
                catch (InvalidOperationException)
                {
                    error = true;
                    // KinectSensor might enter an invalid state while enabling/disabling streams or stream features.
                    // E.g.: sensor might be abruptly unplugged.
                }
            }
            if (!error)
            {
                kinectSensor = args.NewSensor;
                kinectRegion.KinectSensor = args.NewSensor;
            }
        }

        public static KinectSingleton Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new KinectSingleton();
                    }
                }

                return instance;
            }
        }
    }
}
